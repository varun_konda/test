# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0055_feebystudent'),
    ]

    operations = [
        migrations.CreateModel(
            name='ExceptDeduct',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
                ('created_on', models.DateTimeField(auto_now=True)),
                ('updated_on', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='FeeCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=20)),
                ('created_on', models.DateTimeField(auto_now=True)),
                ('updated_on', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='fee',
            name='class_id',
        ),
        migrations.RemoveField(
            model_name='fee',
            name='fee_type',
        ),
        migrations.RemoveField(
            model_name='feebystudent',
            name='fee_name',
        ),
        migrations.RemoveField(
            model_name='feebystudent',
            name='student_id',
        ),
        migrations.DeleteModel(
            name='Fee',
        ),
        migrations.DeleteModel(
            name='FeeByStudent',
        ),
    ]
