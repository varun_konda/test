# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0059_fee'),
    ]

    operations = [
        migrations.CreateModel(
            name='ModeOfPay',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
                ('created_on', models.DateTimeField(auto_now=True)),
                ('updated_on', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='fee',
            name='class_id',
        ),
        migrations.RemoveField(
            model_name='fee',
            name='except_deduct_id',
        ),
        migrations.RemoveField(
            model_name='fee',
            name='fee_category_id',
        ),
        migrations.RemoveField(
            model_name='fee',
            name='fee_type_id',
        ),
        migrations.DeleteModel(
            name='Fee',
        ),
    ]
