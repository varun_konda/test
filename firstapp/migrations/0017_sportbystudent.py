# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0016_auto_20151124_1315'),
    ]

    operations = [
        migrations.CreateModel(
            name='SportByStudent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_active', models.BooleanField()),
                ('created_on', models.DateTimeField(auto_now=True)),
                ('updated_on', models.DateTimeField(auto_now_add=True)),
                ('sports_id', models.ForeignKey(to='firstapp.Sports')),
                ('student_id', models.ForeignKey(to='firstapp.Student')),
            ],
        ),
    ]
