# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0013_clsvicesubjects'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Students',
        ),
    ]
