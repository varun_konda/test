# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0049_auto_20151204_0633'),
    ]

    operations = [
        migrations.CreateModel(
            name='FeeByStudent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('paid', models.PositiveIntegerField()),
                ('due', models.PositiveIntegerField()),
                ('created_on', models.DateTimeField(auto_now=True)),
                ('updated_on', models.DateTimeField(auto_now_add=True)),
                ('student_id', models.ForeignKey(to='firstapp.Student')),
            ],
        ),
        migrations.RemoveField(
            model_name='feebystudentrecords',
            name='student_id',
        ),
        migrations.DeleteModel(
            name='FeeByStudentRecords',
        ),
    ]
