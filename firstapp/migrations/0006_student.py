# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0005_auto_20151123_1359'),
    ]

    operations = [
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('student_name', models.CharField(max_length=50)),
                ('father_name', models.CharField(max_length=50)),
                ('dob', models.DateField(verbose_name=b'date of birth')),
                ('address', models.CharField(max_length=100)),
                ('class_name', models.CharField(max_length=50)),
                ('previous_class', models.CharField(max_length=10)),
                ('previous_school_name', models.CharField(max_length=50)),
            ],
        ),
    ]
