# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0026_auto_20151125_1241'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='exambysubject',
            name='name',
        ),
        migrations.AddField(
            model_name='exambysubject',
            name='examhall_id',
            field=models.ForeignKey(default=2, to='firstapp.ClassRoom'),
            preserve_default=False,
        ),
    ]
