# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0057_fee'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='fee',
            name='except_deduct_id',
        ),
        migrations.RemoveField(
            model_name='fee',
            name='fee_category_id',
        ),
        migrations.RemoveField(
            model_name='fee',
            name='fee_type_id',
        ),
        migrations.RemoveField(
            model_name='fee',
            name='student_id',
        ),
        migrations.DeleteModel(
            name='Fee',
        ),
    ]
