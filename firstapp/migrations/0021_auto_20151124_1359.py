# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0020_qualification'),
    ]

    operations = [
        migrations.AlterField(
            model_name='staff',
            name='qualification',
            field=models.ForeignKey(to='firstapp.Qualification'),
        ),
    ]
