# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0023_classbystaff_period'),
    ]

    operations = [
        migrations.CreateModel(
            name='ExamHall',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('capacity', models.IntegerField()),
                ('exam_type', models.CharField(max_length=20)),
                ('created_on', models.DateTimeField(auto_now=True)),
                ('updated_on', models.DateTimeField(auto_now_add=True)),
                ('classroom_id', models.ForeignKey(to='firstapp.ClassRoom')),
            ],
        ),
    ]
