# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0022_classbystaff'),
    ]

    operations = [
        migrations.AddField(
            model_name='classbystaff',
            name='period',
            field=models.TimeField(null=True, blank=True),
        ),
    ]
