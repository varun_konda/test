# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0054_auto_20151204_0902'),
    ]

    operations = [
        migrations.CreateModel(
            name='FeeByStudent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('paid', models.PositiveIntegerField()),
                ('due', models.PositiveIntegerField()),
                ('created_on', models.DateTimeField(auto_now=True)),
                ('updated_on', models.DateTimeField(auto_now_add=True)),
                ('fee_name', models.ForeignKey(to='firstapp.Fee')),
                ('student_id', models.ForeignKey(to='firstapp.Student')),
            ],
        ),
    ]
