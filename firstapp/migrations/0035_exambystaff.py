# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0034_exambysubject'),
    ]

    operations = [
        migrations.CreateModel(
            name='ExamByStaff',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('attendence', models.BooleanField()),
                ('created_on', models.DateTimeField(auto_now=True)),
                ('updated_on', models.DateTimeField(auto_now_add=True)),
                ('exambysub_id', models.ForeignKey(to='firstapp.ExamHall')),
                ('staff_id', models.ForeignKey(to='firstapp.Staff')),
            ],
        ),
    ]
