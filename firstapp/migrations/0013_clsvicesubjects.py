# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0012_delete_clsvicesubjects'),
    ]

    operations = [
        migrations.CreateModel(
            name='ClsViceSubjects',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now=True)),
                ('updated_on', models.DateTimeField(auto_now_add=True)),
                ('class_id', models.ForeignKey(to='firstapp.ClassRoom')),
                ('subject_id', models.ForeignKey(to='firstapp.Subjects')),
            ],
        ),
    ]
