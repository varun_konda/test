# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0006_student'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Student',
        ),
    ]
