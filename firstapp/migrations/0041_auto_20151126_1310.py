# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0040_feebystudent'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='fee',
            name='class_id',
        ),
        migrations.RemoveField(
            model_name='feebystudent',
            name='fee_id',
        ),
        migrations.RemoveField(
            model_name='feebystudent',
            name='student_id',
        ),
        migrations.DeleteModel(
            name='Fee',
        ),
        migrations.DeleteModel(
            name='FeeByStudent',
        ),
    ]
