# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0044_feebystudent'),
    ]

    operations = [
        migrations.RenameField(
            model_name='fee',
            old_name='fee_id',
            new_name='class_id',
        ),
        migrations.AlterField(
            model_name='fee',
            name='cost',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='feebystudent',
            name='due',
            field=models.PositiveIntegerField(),
        ),
        migrations.AlterField(
            model_name='feebystudent',
            name='paid',
            field=models.PositiveIntegerField(),
        ),
    ]
