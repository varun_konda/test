# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0014_delete_students'),
    ]

    operations = [
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('father_name', models.CharField(max_length=50)),
                ('dob', models.DateField(verbose_name=b'date of birth')),
                ('address', models.CharField(max_length=100)),
                ('previous_class', models.CharField(max_length=10)),
                ('previous_school_name', models.CharField(max_length=50)),
                ('gender', models.CharField(max_length=1, choices=[(b'm', b'Male'), (b'f', b'Female')])),
                ('created_on', models.DateTimeField(auto_now=True)),
                ('updated_on', models.DateTimeField(auto_now_add=True)),
                ('class_id', models.ForeignKey(to='firstapp.ClassRoom')),
            ],
        ),
    ]
