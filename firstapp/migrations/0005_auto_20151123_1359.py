# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0004_class'),
    ]

    operations = [
        migrations.CreateModel(
            name='ClassRoom',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cls_name', models.CharField(max_length=50)),
                ('is_active', models.BooleanField()),
                ('exam_capacity', models.IntegerField()),
                ('req_staff', models.IntegerField()),
                ('staff_cls_room', models.IntegerField()),
                ('staff_exam_room', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='ClsViceSubjects',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('no_of_subjects', models.IntegerField()),
                ('subject_name', models.CharField(max_length=50)),
                ('cls_name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Staff',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('staff_id', models.IntegerField()),
                ('staff_name', models.CharField(max_length=50)),
                ('higher_education', models.CharField(max_length=50)),
                ('experience', models.IntegerField()),
                ('subjects', models.CharField(max_length=50)),
                ('staff_contact_no', models.IntegerField()),
                ('profile_pic', models.CharField(max_length=50)),
                ('date_of_birth', models.DateField()),
                ('remarks', models.CharField(max_length=50)),
                ('prev_school_name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='StaffClsRoom',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cls_name', models.CharField(max_length=50)),
                ('subject_name', models.CharField(max_length=50)),
                ('staff_name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='StaffSsubjects',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('staff_name', models.CharField(max_length=50)),
                ('subject_name', models.CharField(max_length=50)),
                ('upto_class', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Students',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('student_id', models.IntegerField()),
                ('student_name', models.CharField(max_length=50)),
                ('cls_name', models.CharField(max_length=50)),
                ('staff_name', models.CharField(max_length=50)),
                ('exams', models.CharField(max_length=50)),
                ('sports', models.CharField(max_length=50)),
                ('grade', models.CharField(max_length=50)),
                ('father_name', models.CharField(max_length=50)),
                ('prev_school_name', models.CharField(max_length=50)),
                ('prev_cls_marks', models.IntegerField()),
                ('date_of_birth', models.DateField()),
                ('gender', models.BooleanField()),
                ('remarks', models.CharField(max_length=50)),
                ('identification_marks', models.CharField(max_length=50)),
                ('profile_pic', models.CharField(max_length=50)),
                ('student_contact_no', models.IntegerField()),
                ('address', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='TeachingRoom',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cls_no', models.IntegerField()),
                ('cls_name', models.CharField(max_length=50)),
                ('is_active', models.BooleanField()),
                ('cls_capacity', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Timing',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cls_name', models.CharField(max_length=50)),
                ('staff_name', models.CharField(max_length=50)),
                ('subject_name', models.CharField(max_length=50)),
                ('time', models.TimeField()),
            ],
        ),
        migrations.DeleteModel(
            name='Class',
        ),
        migrations.DeleteModel(
            name='Student',
        ),
    ]
