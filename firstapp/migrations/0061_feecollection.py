# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0060_auto_20151204_1101'),
    ]

    operations = [
        migrations.CreateModel(
            name='FeeCollection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.PositiveIntegerField()),
                ('fine', models.PositiveIntegerField()),
                ('discount', models.PositiveIntegerField()),
                ('total_amount', models.PositiveIntegerField()),
                ('created_on', models.DateTimeField(auto_now=True)),
                ('updated_on', models.DateTimeField(auto_now_add=True)),
                ('class_id', models.ForeignKey(to='firstapp.ClassRoom')),
                ('fee_category', models.ForeignKey(to='firstapp.FeeCategory')),
                ('mode_of_pay', models.ForeignKey(to='firstapp.ModeOfPay')),
                ('student_id', models.ForeignKey(to='firstapp.Student')),
            ],
        ),
    ]
