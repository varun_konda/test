# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0039_fee'),
    ]

    operations = [
        migrations.CreateModel(
            name='FeeByStudent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('paid', models.IntegerField()),
                ('due', models.IntegerField()),
                ('fee_id', models.ForeignKey(to='firstapp.Fee')),
                ('student_id', models.ForeignKey(to='firstapp.Student')),
            ],
        ),
    ]
