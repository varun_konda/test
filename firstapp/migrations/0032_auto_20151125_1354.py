# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0031_auto_20151125_1335'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='exambystaff',
            name='exambysub_id',
        ),
        migrations.RemoveField(
            model_name='exambystaff',
            name='staff_id',
        ),
        migrations.RemoveField(
            model_name='exambystudent',
            name='exambysub_id',
        ),
        migrations.RemoveField(
            model_name='exambystudent',
            name='student_id',
        ),
        migrations.RemoveField(
            model_name='exambysubject',
            name='examhall_id',
        ),
        migrations.RemoveField(
            model_name='exambysubject',
            name='subject_id',
        ),
        migrations.RemoveField(
            model_name='examhall',
            name='classroom_id',
        ),
        migrations.DeleteModel(
            name='ExamType',
        ),
        migrations.DeleteModel(
            name='ExamByStaff',
        ),
        migrations.DeleteModel(
            name='ExamByStudent',
        ),
        migrations.DeleteModel(
            name='ExamBySubject',
        ),
        migrations.DeleteModel(
            name='ExamHall',
        ),
    ]
