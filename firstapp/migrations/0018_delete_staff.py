# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0017_sportbystudent'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Staff',
        ),
    ]
