# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0037_fee'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='fee',
            name='class_id',
        ),
        migrations.DeleteModel(
            name='Fee',
        ),
    ]
