# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0047_remove_feebystudent_class_id'),
    ]

    operations = [
        migrations.CreateModel(
            name='FeeByStudentRecords',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('paid', models.PositiveIntegerField()),
                ('due', models.PositiveIntegerField()),
                ('student_id', models.ForeignKey(to='firstapp.Student')),
            ],
        ),
    ]
