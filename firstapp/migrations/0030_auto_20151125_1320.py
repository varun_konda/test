# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0029_auto_20151125_1252'),
    ]

    operations = [
        migrations.CreateModel(
            name='ExamByStaff',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('attendence', models.BooleanField()),
                ('created_on', models.DateTimeField(auto_now=True)),
                ('updated_on', models.DateTimeField(auto_now_add=True)),
                ('exambysub_id', models.ForeignKey(to='firstapp.ClassRoom')),
                ('staff_id', models.ForeignKey(to='firstapp.Staff')),
            ],
        ),
        migrations.CreateModel(
            name='ExamByStudent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('attendence', models.BooleanField()),
                ('marks', models.IntegerField()),
                ('remarks', models.CharField(max_length=50)),
                ('created_on', models.DateTimeField(auto_now=True)),
                ('updated_on', models.DateTimeField(auto_now_add=True)),
                ('exambysub_id', models.ForeignKey(to='firstapp.ClassRoom')),
                ('student_id', models.ForeignKey(to='firstapp.Student')),
            ],
        ),
        migrations.RemoveField(
            model_name='staffbyexam',
            name='exambysub_id',
        ),
        migrations.RemoveField(
            model_name='staffbyexam',
            name='staff_id',
        ),
        migrations.AddField(
            model_name='exambysubject',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 25, 13, 20, 49, 228477, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='exambysubject',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 25, 13, 20, 54, 260105, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.DeleteModel(
            name='StaffByExam',
        ),
    ]
