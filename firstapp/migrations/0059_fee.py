# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0058_auto_20151204_1015'),
    ]

    operations = [
        migrations.CreateModel(
            name='Fee',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.PositiveIntegerField(default=0)),
                ('created_on', models.DateTimeField(auto_now=True)),
                ('updated_on', models.DateTimeField(auto_now_add=True)),
                ('class_id', models.ForeignKey(to='firstapp.ClassRoom')),
                ('except_deduct_id', models.ForeignKey(to='firstapp.ExceptDeduct')),
                ('fee_category_id', models.ForeignKey(to='firstapp.FeeCategory')),
                ('fee_type_id', models.ForeignKey(to='firstapp.FeeType')),
            ],
        ),
    ]
