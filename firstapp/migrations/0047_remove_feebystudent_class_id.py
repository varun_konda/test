# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0046_auto_20151126_1324'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='feebystudent',
            name='class_id',
        ),
    ]
