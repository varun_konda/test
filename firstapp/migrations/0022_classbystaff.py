# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0021_auto_20151124_1359'),
    ]

    operations = [
        migrations.CreateModel(
            name='ClassByStaff',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now=True)),
                ('updated_on', models.DateTimeField(auto_now_add=True)),
                ('clsvicesubjects_id', models.ForeignKey(to='firstapp.ClsViceSubjects')),
                ('staff_id', models.ForeignKey(to='firstapp.Staff')),
            ],
        ),
    ]
