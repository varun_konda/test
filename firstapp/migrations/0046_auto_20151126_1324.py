# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0045_auto_20151126_1320'),
    ]

    operations = [
        migrations.RenameField(
            model_name='feebystudent',
            old_name='fee_id',
            new_name='class_id',
        ),
    ]
