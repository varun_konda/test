# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0011_classroom'),
    ]

    operations = [
        migrations.DeleteModel(
            name='ClsViceSubjects',
        ),
    ]
