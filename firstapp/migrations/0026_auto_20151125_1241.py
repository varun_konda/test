# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0025_exambysubject'),
    ]

    operations = [
        migrations.RenameField(
            model_name='exambysubject',
            old_name='examhall_id',
            new_name='name',
        ),
    ]
