# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0015_student'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='previous_class',
            field=models.PositiveIntegerField(),
        ),
    ]
