# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0033_auto_20151125_1355'),
    ]

    operations = [
        migrations.CreateModel(
            name='ExamBySubject',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField()),
                ('start_time', models.TimeField()),
                ('end_time', models.TimeField()),
                ('remarks', models.CharField(max_length=50)),
                ('created_on', models.DateTimeField(auto_now=True)),
                ('updated_on', models.DateTimeField(auto_now_add=True)),
                ('examhall_id', models.ForeignKey(to='firstapp.ExamHall')),
                ('subject_id', models.ForeignKey(to='firstapp.Subjects')),
            ],
        ),
    ]
