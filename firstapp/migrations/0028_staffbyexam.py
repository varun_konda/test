# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0027_auto_20151125_1243'),
    ]

    operations = [
        migrations.CreateModel(
            name='StaffByExam',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('attendence', models.BooleanField()),
                ('exambysub_id', models.ForeignKey(to='firstapp.ExamBySubject')),
                ('staff_id', models.ForeignKey(to='firstapp.Staff')),
            ],
        ),
    ]
