# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0028_staffbyexam'),
    ]

    operations = [
        migrations.AlterField(
            model_name='staffbyexam',
            name='exambysub_id',
            field=models.ForeignKey(to='firstapp.ClassRoom'),
        ),
    ]
