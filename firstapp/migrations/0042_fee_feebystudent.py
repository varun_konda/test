# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0041_auto_20151126_1310'),
    ]

    operations = [
        migrations.CreateModel(
            name='Fee',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cost', models.IntegerField()),
                ('created_on', models.DateTimeField(auto_now=True)),
                ('updated_on', models.DateTimeField(auto_now_add=True)),
                ('fee_id', models.ForeignKey(to='firstapp.ClassRoom')),
            ],
        ),
        migrations.CreateModel(
            name='FeeByStudent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('paid', models.IntegerField()),
                ('due', models.IntegerField()),
                ('fee_id', models.ForeignKey(to='firstapp.Fee')),
                ('student_id', models.ForeignKey(to='firstapp.Student')),
            ],
        ),
    ]
