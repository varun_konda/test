# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0035_exambystaff'),
    ]

    operations = [
        migrations.CreateModel(
            name='ExamByStudent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('attendence', models.BooleanField()),
                ('marks', models.IntegerField()),
                ('remarks', models.CharField(max_length=50)),
                ('created_on', models.DateTimeField(auto_now=True)),
                ('updated_on', models.DateTimeField(auto_now_add=True)),
                ('exambysub_id', models.ForeignKey(to='firstapp.ExamHall')),
                ('student_id', models.ForeignKey(to='firstapp.Student')),
            ],
        ),
    ]
