from django.contrib import admin

from .models import (Subjects, Sports, ClassRoom, ClsViceSubjects, Student,
                     SportByStudent, Staff, Qualification, ClassByStaff,
                    ExamType, ExamHall, ExamBySubject, ExamByStaff,
                     ExamByStudent, FeeCategory, FeeType, ExceptDeduct,
                     ModeOfPay, FeeCollection)

from .models import TeachingRoom


# from .models import Student


# class QuestionAdmin(admin.ModelAdmin):
#     #fields = ['question_text', 'pub_date']
#     fieldsets = [
#         (None, {'fields': ['pub_date']}),
#         ('Date information', {'fields': ['question_text'], 'classes': ['collapse']}),
#     ]


class ClassViceSubjectsAdmin(admin.ModelAdmin):
    list_display = ['class_id', 'subject_id']


class StudentAdmin(admin.ModelAdmin):
    list_display = ['name', 'class_id']


class SportsByStudentAdmin(admin.ModelAdmin):
    list_display = ['student_id', 'sports_id', 'is_active']


class ClassByStaffAdmin(admin.ModelAdmin):
    list_display = ['clsvicesubjects_id', 'staff_id', 'period']


# class ExamHallAdmin(admin.ModelAdmin):
#     list_display = ['classroom_id', 'exam_type']


class ExamBySubjectAdmin(admin.ModelAdmin):
    list_display = ['examhall_id', 'subject_id','date']


class ExamByStaffAdmin(admin.ModelAdmin):
    list_display = ['exambysub_id', 'staff_id', 'attendence']


class ExamByStudentAdmin(admin.ModelAdmin):
    list_display = ['exambysub_id', 'student_id', 'attendence', 'marks']


class FeeAdmin(admin.ModelAdmin):
    list_display = ['fee_category_id', 'class_id', 'amount']


class FeeCollectionAdmin(admin.ModelAdmin):
    list_display = ['class_id', 'student_id', 'fee_category', 'total_amount',
                    'mode_of_pay']

# admin.site.register(Question, QuestionAdmin)
# admin.site.register(Choice)
admin.site.register(ClassRoom)
admin.site.register(TeachingRoom)
admin.site.register(Staff)
admin.site.register(ClsViceSubjects, ClassViceSubjectsAdmin)
# admin.site.register(StaffSsubjects)
# admin.site.register(StaffClsRoom)
# admin.site.register(Students)
admin.site.register(Student)
admin.site.register(Subjects)
admin.site.register(Sports)
admin.site.register(SportByStudent, SportsByStudentAdmin)
admin.site.register(Qualification)
admin.site.register(ClassByStaff, ClassByStaffAdmin)
admin.site.register(ExamHall)
admin.site.register(ExamBySubject, ExamBySubjectAdmin)
admin.site.register(ExamByStaff, ExamByStaffAdmin)
admin.site.register(ExamByStudent, ExamByStudentAdmin)
admin.site.register(ExamType)
admin.site.register(FeeCategory)
admin.site.register(FeeType)
admin.site.register(ExceptDeduct)
admin.site.register(ModeOfPay)
admin.site.register(FeeCollection,FeeCollectionAdmin)
# admin.site.register(Fee, FeeAdmin)
# admin.site.register(FeeByStudent, FeeByStudentAdmin)

# Register your models here.
