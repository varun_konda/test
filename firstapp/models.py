from django.conf import settings
from django.db import models


class Question(models.Model):
    question_text = models.CharField(max_length=100)
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        return self.question_text


class Choice(models.Model):
    question = models.ForeignKey(Question)
    choice_text = models.CharField(max_length=100)
    votes = models.IntegerField(default=0)

    def _str_(self):
        return self.choice_text

    def __unicode__(self):
        return self.choice_text


class ClassRoom(models.Model):
    name = models.CharField(max_length=50)
    # is_active = models.BooleanField()
    # exam_capacity = models.IntegerField()
    # req_staff = models.IntegerField()
    # staff_cls_room = models.IntegerField()
    # staff_exam_room = models.IntegerField()
    # subjects = list sub[]
    created_on = models.DateTimeField(auto_now=True)
    updated_on = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.name


class TeachingRoom(models.Model):
    cls_name = models.CharField(max_length=50)
    is_active = models.BooleanField()
    cls_capacity = models.IntegerField()

    def __unicode__(self):
        return self.cls_name


class StaffSsubjects(models.Model):
    staff_name = models.CharField(max_length=50)
    subject_name = models.CharField(max_length=50)
    upto_class = models.CharField(max_length=50)


class StaffClsRoom(models.Model):
    cls_name = models.CharField(max_length=50)
    subject_name = models.CharField(max_length=50)
    staff_name = models.CharField(max_length=50)


# class Students(models.Model):
#     student_id = models.IntegerField()
#     student_name = models.CharField(max_length=50)
#     cls_name = models.CharField(max_length=50)
#     staff_name = models.CharField(max_length=50)
#     exams = models.CharField(max_length=50)
#     sports = models.CharField(max_length=50)
#     grade = models.CharField(max_length=50)
#     father_name = models.CharField(max_length=50)
#     prev_school_name = models.CharField(max_length=50)
#     prev_cls_marks = models.IntegerField()
#     date_of_birth = models.DateField()
#     gender = models.BooleanField()
#     remarks = models.CharField(max_length=50)
#     identification_marks = models.CharField(max_length=50)
#     profile_pic = models.CharField(max_length=50)
#     student_contact_no = models.IntegerField()
#     address = models.CharField(max_length=50)


class Timing(models.Model):
    cls_name = models.CharField(max_length=50)
    staff_name = models.CharField(max_length=50)
    subject_name = models.CharField(max_length=50)
    time = models.TimeField()


class Subjects(models.Model):
    subject_name = models.CharField(max_length=15)

    def __unicode__(self):
        return self.subject_name


class Sports(models.Model):
    name = models.CharField(max_length=15)
    created_on = models.DateTimeField(auto_now=True)
    updated_on = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.name


class ClsViceSubjects(models.Model):
    # cls_name = models.CharField(max_length=50)
    # no_of_subjects = models.IntegerField()
    # subject_name = models.CharField(max_length=50)
    class_id = models.ForeignKey(ClassRoom)
    subject_id = models.ForeignKey(Subjects)
    created_on = models.DateTimeField(auto_now=True)
    updated_on = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.class_id.name + self.subject_id.subject_name


class Student(models.Model):
    class_id = models.ForeignKey(ClassRoom)
    name = models.CharField(max_length=50)
    father_name = models.CharField(max_length=50)
    dob = models.DateField('date of birth')
    address = models.CharField(max_length=100)
    previous_class = models.PositiveIntegerField()
    previous_school_name = models.CharField(max_length=50)
    gender = models.CharField(max_length=1, choices=settings.GENDER_CHOICES)
    created_on = models.DateTimeField(auto_now=True)
    updated_on = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.name


class SportByStudent(models.Model):
    student_id = models.ForeignKey(Student)
    sports_id = models.ForeignKey(Sports)
    is_active = models.BooleanField()
    created_on = models.DateTimeField(auto_now=True)
    updated_on = models.DateTimeField(auto_now_add=True)


class Qualification(models.Model):
    name = models.CharField(max_length=20)

    def __unicode__(self):
        return self.name


class Staff(models.Model):
    name = models.CharField(max_length=50)
    gender = models.CharField(max_length=1, choices=settings.GENDER_CHOICES)
    experience = models.IntegerField()
    qualification = models.ForeignKey(Qualification)
    contact_no = models.IntegerField()
    profile_pic = models.CharField(max_length=50)
    dob = models.DateField()
    address = models.CharField(max_length=100)
    remarks = models.CharField(max_length=50)
    prev_school_name = models.CharField(max_length=50)
    created_on = models.DateTimeField(auto_now=True)
    updated_on = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.name


class ClassByStaff(models.Model):
    clsvicesubjects_id = models.ForeignKey(ClsViceSubjects)
    staff_id = models.ForeignKey(Staff)
    period = models.TimeField(null=True, blank=True)
    created_on = models.DateTimeField(auto_now=True)
    updated_on = models.DateTimeField(auto_now_add=True)


class ExamType(models.Model):
    name = models.CharField(max_length=20)

    def __unicode__(self):
        return self.name


class ExamHall(models.Model):
    classroom_id = models.ForeignKey(ClassRoom)
    capacity = models.IntegerField()
    exam_type = models.ForeignKey(ExamType)
    created_on = models.DateTimeField(auto_now=True)
    updated_on = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.classroom_id.name + self.exam_type.name


class ExamBySubject(models.Model):
    examhall_id = models.ForeignKey(ExamHall)
    subject_id = models.ForeignKey(Subjects)
    date = models.DateField()
    start_time = models.TimeField()
    end_time = models.TimeField()
    remarks = models.CharField(max_length=50)
    created_on = models.DateTimeField(auto_now=True)
    updated_on = models.DateTimeField(auto_now_add=True)


class ExamByStaff(models.Model):
    exambysub_id = models.ForeignKey(ExamHall)
    staff_id = models.ForeignKey(Staff)
    attendence = models.BooleanField()
    created_on = models.DateTimeField(auto_now=True)
    updated_on = models.DateTimeField(auto_now_add=True)


class ExamByStudent(models.Model):
    exambysub_id = models.ForeignKey(ExamHall)
    student_id = models.ForeignKey(Student)
    attendence = models.BooleanField()
    marks = models.IntegerField()
    remarks = models.CharField(max_length=50)
    created_on = models.DateTimeField(auto_now=True)
    updated_on = models.DateTimeField(auto_now_add=True)


class FeeCategory(models.Model):
    name = models.CharField(max_length=20)
    created_on = models.DateTimeField(auto_now=True)
    updated_on = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.name


class FeeType(models.Model):
    name = models.CharField(max_length=30)
    created_on = models.DateTimeField(auto_now=True)
    updated_on = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.name


class ExceptDeduct(models.Model):
    name = models.CharField(max_length=30)
    created_on = models.DateTimeField(auto_now=True)
    updated_on = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.name


class ModeOfPay(models.Model):
    name = models.CharField(max_length=30)
    created_on = models.DateTimeField(auto_now=True)
    updated_on = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.name


class FeeCollection(models.Model):
    class_id = models.ForeignKey(ClassRoom)
    student_id = models.ForeignKey(Student)
    fee_category = models.ForeignKey(FeeCategory)
    amount = models.PositiveIntegerField()
    fine = models.PositiveIntegerField()
    discount = models.PositiveIntegerField()
    total_amount = models.PositiveIntegerField()
    mode_of_pay = models.ForeignKey(ModeOfPay)
    created_on = models.DateTimeField(auto_now=True)
    updated_on = models.DateTimeField(auto_now_add=True)


# class Fee(models.Model):
#     fee_category_id = models.ForeignKey(FeeCategory)
#     class_id = models.ForeignKey(ClassRoom)
#     amount = models.PositiveIntegerField(default=0)
#     except_deduct_id = models.ForeignKey(ExceptDeduct)
#     fee_type_id = models.ForeignKey(FeeType)
#     created_on = models.DateTimeField(auto_now=True)
#     updated_on = models.DateTimeField(auto_now_add=True)

#
#
# class FeeByStudent(models.Model):
#     fee_name = models.ForeignKey(Fee)
#     student_id = models.ForeignKey(Student)
#     # class_id = models.ForeignKey(Fee)
#     paid = models.PositiveIntegerField()
#     due = models.PositiveIntegerField()
#     created_on = models.DateTimeField(auto_now=True)
#     updated_on = models.DateTimeField(auto_now_add=True)




# Create your models here.
